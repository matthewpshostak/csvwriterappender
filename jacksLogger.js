const fs = require('fs')
const dateFormat = require('dateformat')
const now = new Date()
const today = dateFormat(now, "mm/dd/yy")
const monthYear = dateFormat("mmmm yyyy")
const csvName = monthYear + ' ShipProtectionLog.csv'
let data = {
  date: today,
  ticket: Math.floor(100000 + Math.random() * 900000),
  protection: Math.floor(Math.random() * (1000 - 100) + 100) / 100
}
const firstRow = 'Date,Ticket,Protection' + '\n'

new Promise (function(resolve) {
  try {
    fs.exists(csvName, (exists) => {
      if (exists == false) {
        console.log('No running log yet for this month, creating new!')  
        resolve(fs.writeFileSync(csvName, firstRow))   
      } else {
        resolve(console.log('Log already exists for the month.'))
      }
    })
  } catch (error) {
    console.log(error)
  }
  
}).then(function() {
  try {
    fs.appendFileSync(csvName, convertObjectToCSV(data) + '\n')
    console.log('The data was appended to file!')
  } catch (err) {
    /* Handle the error */
  }
})


const convertObjectToCSV = function (data, columnDelimiter = ',', lineDelimiter = '\n') {
  data = Array.isArray(data)? data: [data]
  let keys = Object.keys(data[0])
  return data.map(item => keys.map(key => {
      let val = item[key] || ""
      if (columnDelimiter === ',' && typeof val === "string" && val.indexOf(columnDelimiter) > -1) {
        val = `"${val}"`
      }
      return val
    }).join(columnDelimiter)
  ).join(lineDelimiter)
}







